
A GROUP is a set in which you can perform one operation (usually
addition or multiplication mod n for us) with some nice properties. A RING is a set equipped
with two operations, called addition and multiplication. A RING is a GROUP under addition
and satisfies some of the properties of a group for multiplication. A FIELD is a GROUP
under both addition and multiplication.

# Primes

Prime numbers are whole numbers larger than 1 that are not divisible by any whole number apart from 1 and themselves. They’re like the atoms of number theory — you can use primes to make any other number.

Among the first billion prime numbers, for instance, a prime ending in 9 is almost 65 percent more likely to be followed by a prime ending in 1 than another prime ending in 9. In a paper posted online today, Kannan Soundararajan and Robert Lemke Oliver of Stanford University present both numerical and theoretical evidence that prime numbers repel other would-be primes that end in the same digit, and have varied predilections for being followed by primes ending in the other possible final digits.
This conspiracy among prime numbers seems, at first glance, to violate a longstanding assumption in number theory: that prime numbers behave much like random numbers. Most mathematicians would have assumed, Granville and Ono agreed, that a prime should have an equal chance of being followed by a prime ending in 1, 3, 7 or 9 (the four possible endings for all prime numbers except 2 and 5).
The primes’ preferences about the final digits of the primes that follow them can be explained, Soundararajan and Lemke Oliver found, using a much more refined model of randomness in primes, something called the prime k-tuples conjecture. Originally stated by mathematicians G. H. Hardy and J. E. Littlewood in 1923, the conjecture provides precise estimates of how often every possible constellation of primes with a given spacing pattern will appear. A wealth of numerical evidence supports the conjecture, but so far a proof has eluded mathematicians.



In the 17th century, mathematicians discovered a curious dichotomy: If you ignore 2, primes that are a sum of two squares (for example, 73 = 82 + 32) are always 1 more than a multiple of four, while primes that are not a sum of two squares (such as 7 and 11) are always 1 less than a multiple of four. This is known as the two-squares theorem. But why should this be? Such questions can be understood using a version of arithmetic in larger number systems.