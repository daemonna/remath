FROM fedora:latest

RUN dnf install cargo rust -y
COPY . /app
RUN cd /app && cargo build
CMD /app/target/debug/remath