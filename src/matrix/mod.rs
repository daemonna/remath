struct Matrix {
    cols: i32,
    rows: i32,
    data: Vec<f32>
}

impl Matrix {
    fn new(cols: i32, rows: i32) -> Matrix {
        Matrix {
            cols: cols,
            rows: rows,
            data: vec![0.0; cols * rows]
        }
    }
}