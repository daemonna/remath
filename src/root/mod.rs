// mod html;

pub fn get_root()->String {
    let contents = r#"
    <!DOCTYPE html>
    <html lang="en" class="pf-m-redhat-font">
    <head>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <!-- <link rel="stylesheet" href="fonts.css" />
       Include latest PatternFly CSS via CDN -->
       <link rel="stylesheet" href="/patternfly.css">
      <link rel="stylesheet" href="style.css" />
      <title>REMATH</title>
    </head>
    <body>
      <div class="pf-c-page" id="primary-detail-expanded-example">
      <a class="pf-c-skip-to-content pf-c-button pf-m-primary" href="\#main-content-primary-detail-expanded-example">Skip to content</a>
      <header class="pf-c-page__header">
        <div class="pf-c-page__header-brand">
          <div class="pf-c-page__header-brand-toggle">
            <button class="pf-c-button pf-m-plain" type="button" id="primary-detail-expanded-example-nav-toggle" aria-label="Global navigation" aria-expanded="true" aria-controls="primary-detail-expanded-example-primary-nav">
              <i class="fas fa-bars" aria-hidden="true"></i>
            </button>
          </div>
          <a href="\#" class="pf-c-page__header-brand-link">
            <img class="pf-c-brand" src="/assets/images/PF-Masthead-Logo.svg" alt="PatternFly logo" />
          </a>
        </div>
        <div class="pf-c-page__header-tools">
          <div class="pf-c-page__header-tools-group">
            <div class="pf-c-page__header-tools-item pf-m-hidden pf-m-visible-on-lg ">
              <button class="pf-c-button pf-m-plain" type="button" aria-label="Settings">
                <i class="fas fa-cog" aria-hidden="true"></i>
              </button>
            </div>
            <div class="pf-c-page__header-tools-item pf-m-hidden pf-m-visible-on-lg">
              <button class="pf-c-button pf-m-plain" type="button" aria-label="Help">
                <i class="pf-icon pf-icon-help" aria-hidden="true"></i>
              </button>
            </div>
          </div>
          <div class="pf-c-page__header-tools-group">
            <div class="pf-c-page__header-tools-item pf-m-hidden-on-lg">
              <div class="pf-c-dropdown">
                <button class="pf-c-dropdown__toggle pf-m-plain" id="primary-detail-expanded-example-dropdown-kebab-1-button" aria-expanded="false" type="button" aria-label="Actions">
                  <i class="fas fa-ellipsis-v" aria-hidden="true"></i>
                </button>
                <ul class="pf-c-dropdown__menu pf-m-align-right" aria-labelledby="primary-detail-expanded-example-dropdown-kebab-1-button" hidden>
                  <li>
                    <a class="pf-c-dropdown__menu-item" href="\#">Link</a>
                  </li>
                  <li>
                    <button class="pf-c-dropdown__menu-item" type="button">Action</button>
                  </li>
                  <li>
                    <a class="pf-c-dropdown__menu-item pf-m-disabled" href="\#" aria-disabled="true" tabindex="-1">Disabled link</a>
                  </li>
                  <li>
                    <button class="pf-c-dropdown__menu-item" type="button" disabled>Disabled action</button>
                  </li>
                  <li class="pf-c-divider" role="separator"></li>
                  <li>
                    <a class="pf-c-dropdown__menu-item" href="\#">Separated link</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="pf-c-page__header-tools-item pf-m-hidden pf-m-visible-on-md">
              <div class="pf-c-dropdown">
                <button class="pf-c-dropdown__toggle" id="primary-detail-expanded-example-dropdown-kebab-2-button" aria-expanded="false" type="button">
                  <span class="pf-c-dropdown__toggle-text">John Smith</span>
                  <span class="pf-c-dropdown__toggle-icon">
                    <i class="fas fa-caret-down" aria-hidden="true"></i>
                  </span>
                </button>
                <ul class="pf-c-dropdown__menu" aria-labelledby="primary-detail-expanded-example-dropdown-kebab-2-button" hidden>
                  <li>
                    <a class="pf-c-dropdown__menu-item" href="\#">Link</a>
                  </li>
                  <li>
                    <button class="pf-c-dropdown__menu-item" type="button">Action</button>
                  </li>
                  <li>
                    <a class="pf-c-dropdown__menu-item pf-m-disabled" href="\#" aria-disabled="true" tabindex="-1">Disabled link</a>
                  </li>
                  <li>
                    <button class="pf-c-dropdown__menu-item" type="button" disabled>Disabled action</button>
                  </li>
                  <li class="pf-c-divider" role="separator"></li>
                  <li>
                    <a class="pf-c-dropdown__menu-item" href="\#">Separated link</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <img class="pf-c-avatar" src="/assets/images/img_avatar.svg" alt="Avatar image" />
        </div>
      </header>
      <div class="pf-c-page__sidebar">
        <div class="pf-c-page__sidebar-body">
          <nav class="pf-c-nav" id="primary-detail-expanded-example-primary-nav" aria-label="Global">
            <ul class="pf-c-nav__list">
              <li class="pf-c-nav__item">
                <a href="\#" class="pf-c-nav__link pf-m-current" aria-current="page">System panel</a>
              </li>
              <li class="pf-c-nav__item">
                <a href="\#" class="pf-c-nav__link">Policy</a>
              </li>
              <li class="pf-c-nav__item">
                <a href="\#" class="pf-c-nav__link">Authentication</a>
              </li>
              <li class="pf-c-nav__item">
                <a href="\#" class="pf-c-nav__link">Network services</a>
              </li>
              <li class="pf-c-nav__item">
                <a href="\#" class="pf-c-nav__link">Server</a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
      <main class="pf-c-page__main" tabindex="-1">
        <section class="pf-c-page__main-breadcrumb pf-m-limit-width">
          <div class="pf-c-page__main-body">
            <nav class="pf-c-breadcrumb" aria-label="breadcrumb">
              <ol class="pf-c-breadcrumb__list">
                <li class="pf-c-breadcrumb__item">
                  <a href="\#" class="pf-c-breadcrumb__link">Section home</a>
                </li>
                <li class="pf-c-breadcrumb__item">
                  <span class="pf-c-breadcrumb__item-divider">
                    <i class="fas fa-angle-right" aria-hidden="true"></i>
                  </span>
                  <a href="\#" class="pf-c-breadcrumb__link">Section title</a>
                </li>
                <li class="pf-c-breadcrumb__item">
                  <span class="pf-c-breadcrumb__item-divider">
                    <i class="fas fa-angle-right" aria-hidden="true"></i>
                  </span>
                  <a href="\#" class="pf-c-breadcrumb__link">Section title</a>
                </li>
                <li class="pf-c-breadcrumb__item">
                  <span class="pf-c-breadcrumb__item-divider">
                    <i class="fas fa-angle-right" aria-hidden="true"></i>
                  </span>
                  <a href="\#" class="pf-c-breadcrumb__link pf-m-current" aria-current="page">Section landing</a>
                </li>
              </ol>
            </nav>
          </div>
        </section>
        <section class="pf-c-page__main-section pf-m-light">
          <div class="pf-c-content">
            <h1>Primary-detail expanded</h1>
            <p>Body text should be Red Hat Text Regular at 16px. It should have leading of 24px because of its relative line height of 1.5</p>
          </div>
        </section>
        <div class="pf-c-divider" role="separator"></div>
        <section class="pf-c-page__main-section pf-m-no-padding">
          <!-- Drawer -->
          <div class="pf-c-drawer pf-m-expanded pf-m-inline-on-2xl">
            <div class="pf-c-drawer__main">
              <!-- Content -->
              <div class="pf-c-drawer__content">
                <div class="pf-c-drawer__body">
                  <div class="pf-c-toolbar pf-m-page-insets" id="primary-detail-expanded-example-toolbar">
                    <div class="pf-c-toolbar__content">
                      <div class="pf-c-toolbar__content-section pf-m-nowrap">
                        <div class="pf-c-toolbar__group pf-m-toggle-group pf-m-show-on-lg">
                          <div class="pf-c-toolbar__toggle">
                            <button class="pf-c-button pf-m-plain" type="button" aria-label="Show filters" aria-expanded="false" aria-controls="primary-detail-expanded-example-toolbar-expandable-content">
                              <i class="fas fa-filter" aria-hidden="true"></i>
                            </button>
                          </div>
                          <div class="pf-c-toolbar__item pf-m-search-filter ">
                            <div class="pf-c-input-group" aria-label="search filter" role="group">
                              <div class="pf-c-dropdown">
                                <button class="pf-c-dropdown__toggle" id="primary-detail-expanded-example-toolbar--button" aria-expanded="false" type="button">
                                  <span class="pf-c-dropdown__toggle-text">Name</span>
                                  <span class="pf-c-dropdown__toggle-icon">
                                    <i class="fas fa-caret-down" aria-hidden="true"></i>
                                  </span>
                                </button>
                                <ul class="pf-c-dropdown__menu" aria-labelledby="primary-detail-expanded-example-toolbar--button" hidden>
                                  <li>
                                    <a class="pf-c-dropdown__menu-item" href="\#">Link</a>
                                  </li>
                                  <li>
                                    <button class="pf-c-dropdown__menu-item" type="button">Action</button>
                                  </li>
                                  <li>
                                    <a class="pf-c-dropdown__menu-item pf-m-disabled" href="\#" aria-disabled="true" tabindex="-1">Disabled link</a>
                                  </li>
                                  <li>
                                    <button class="pf-c-dropdown__menu-item" type="button" disabled>Disabled action</button>
                                  </li>
                                  <li class="pf-c-divider" role="separator"></li>
                                  <li>
                                    <a class="pf-c-dropdown__menu-item" href="\#">Separated link</a>
                                  </li>
                                </ul>
                              </div>
                              <input class="pf-c-form-control" type="search" id="primary-detail-expanded-example-toolbar--search-filter-input" name="primary-detail-expanded-example-toolbar-search-filter-input" aria-label="input with dropdown and button" aria-describedby="primary-detail-expanded-example-toolbar--button" />
                            </div>
                          </div>
                          <div class="pf-c-toolbar__group pf-m-filter-group">
                            <div class="pf-c-toolbar__item">
                              <div class="pf-c-select">
                                <span id="primary-detail-expanded-example-toolbar-select-checkbox-status-label" hidden>Choose one</span>
                                <button class="pf-c-select__toggle" type="button" id="primary-detail-expanded-example-toolbar-select-checkbox-status-toggle" aria-haspopup="true" aria-expanded="false" aria-labelledby="primary-detail-expanded-example-toolbar-select-checkbox-status-label primary-detail-expanded-example-toolbar-select-checkbox-status-toggle">
                                  <div class="pf-c-select__toggle-wrapper">
                                    <span class="pf-c-select__toggle-text">Status</span>
                                  </div>
                                  <span class="pf-c-select__toggle-arrow">
                                    <i class="fas fa-caret-down" aria-hidden="true"></i>
                                  </span>
                                </button>
                                <div class="pf-c-select__menu" hidden>
                                  <fieldset class="pf-c-select__menu-fieldset" aria-label="Select input">
                                    <label class="pf-c-check pf-c-select__menu-item pf-m-description" for="primary-detail-expanded-example-toolbar-select-checkbox-status-active">
                                      <input class="pf-c-check__input" type="checkbox" type="checkbox" id="primary-detail-expanded-example-toolbar-select-checkbox-status-active" name="primary-detail-expanded-example-toolbar-select-checkbox-status-active" />
                                      <span class="pf-c-check__label">Active</span>
                                      <div class="pf-c-check__description">This is a description</div>
                                    </label>
                                    <label class="pf-c-check pf-c-select__menu-item pf-m-description" for="primary-detail-expanded-example-toolbar-select-checkbox-status-canceled">
                                      <input class="pf-c-check__input" type="checkbox" type="checkbox" id="primary-detail-expanded-example-toolbar-select-checkbox-status-canceled" name="primary-detail-expanded-example-toolbar-select-checkbox-status-canceled" />
                                      <span class="pf-c-check__label">Canceled</span>
                                      <div class="pf-c-check__description">This is a really long description that describes the menu item. This is a really long description that describes the menu item.</div>
                                    </label>
                                    <label class="pf-c-check pf-c-select__menu-item" for="primary-detail-expanded-example-toolbar-select-checkbox-status-paused">
                                      <input class="pf-c-check__input" type="checkbox" type="checkbox" id="primary-detail-expanded-example-toolbar-select-checkbox-status-paused" name="primary-detail-expanded-example-toolbar-select-checkbox-status-paused" />
                                      <span class="pf-c-check__label">Paused</span>
                                    </label>
                                    <label class="pf-c-check pf-c-select__menu-item" for="primary-detail-expanded-example-toolbar-select-checkbox-status-warning">
                                      <input class="pf-c-check__input" type="checkbox" type="checkbox" id="primary-detail-expanded-example-toolbar-select-checkbox-status-warning" name="primary-detail-expanded-example-toolbar-select-checkbox-status-warning" />
                                      <span class="pf-c-check__label">Warning</span>
                                    </label>
                                    <label class="pf-c-check pf-c-select__menu-item" for="primary-detail-expanded-example-toolbar-select-checkbox-status-restarted">
                                      <input class="pf-c-check__input" type="checkbox" type="checkbox" id="primary-detail-expanded-example-toolbar-select-checkbox-status-restarted" name="primary-detail-expanded-example-toolbar-select-checkbox-status-restarted" />
                                      <span class="pf-c-check__label">Restarted</span>
                                    </label>
                                  </fieldset>
                                </div>
                              </div>
                            </div>
                            <div class="pf-c-toolbar__item">
                              <div class="pf-c-select">
                                <span id="primary-detail-expanded-example-toolbar-select-checkbox-risk-label" hidden>Choose one</span>
                                <button class="pf-c-select__toggle" type="button" id="primary-detail-expanded-example-toolbar-select-checkbox-risk-toggle" aria-haspopup="true" aria-expanded="false" aria-labelledby="primary-detail-expanded-example-toolbar-select-checkbox-risk-label primary-detail-expanded-example-toolbar-select-checkbox-risk-toggle">
                                  <div class="pf-c-select__toggle-wrapper">
                                    <span class="pf-c-select__toggle-text">Risk</span>
                                  </div>
                                  <span class="pf-c-select__toggle-arrow">
                                    <i class="fas fa-caret-down" aria-hidden="true"></i>
                                  </span>
                                </button>
                                <div class="pf-c-select__menu" hidden>
                                  <fieldset class="pf-c-select__menu-fieldset" aria-label="Select input">
                                    <label class="pf-c-check pf-c-select__menu-item pf-m-description" for="primary-detail-expanded-example-toolbar-select-checkbox-risk-active">
                                      <input class="pf-c-check__input" type="checkbox" type="checkbox" id="primary-detail-expanded-example-toolbar-select-checkbox-risk-active" name="primary-detail-expanded-example-toolbar-select-checkbox-risk-active" />
                                      <span class="pf-c-check__label">Active</span>
                                      <div class="pf-c-check__description">This is a description</div>
                                    </label>
                                    <label class="pf-c-check pf-c-select__menu-item pf-m-description" for="primary-detail-expanded-example-toolbar-select-checkbox-risk-canceled">
                                      <input class="pf-c-check__input" type="checkbox" type="checkbox" id="primary-detail-expanded-example-toolbar-select-checkbox-risk-canceled" name="primary-detail-expanded-example-toolbar-select-checkbox-risk-canceled" />
                                      <span class="pf-c-check__label">Canceled</span>
                                      <div class="pf-c-check__description">This is a really long description that describes the menu item. This is a really long description that describes the menu item.</div>
                                    </label>
                                    <label class="pf-c-check pf-c-select__menu-item" for="primary-detail-expanded-example-toolbar-select-checkbox-risk-paused">
                                      <input class="pf-c-check__input" type="checkbox" type="checkbox" id="primary-detail-expanded-example-toolbar-select-checkbox-risk-paused" name="primary-detail-expanded-example-toolbar-select-checkbox-risk-paused" />
                                      <span class="pf-c-check__label">Paused</span>
                                    </label>
                                    <label class="pf-c-check pf-c-select__menu-item" for="primary-detail-expanded-example-toolbar-select-checkbox-risk-warning">
                                      <input class="pf-c-check__input" type="checkbox" type="checkbox" id="primary-detail-expanded-example-toolbar-select-checkbox-risk-warning" name="primary-detail-expanded-example-toolbar-select-checkbox-risk-warning" />
                                      <span class="pf-c-check__label">Warning</span>
                                    </label>
                                    <label class="pf-c-check pf-c-select__menu-item" for="primary-detail-expanded-example-toolbar-select-checkbox-risk-restarted">
                                      <input class="pf-c-check__input" type="checkbox" type="checkbox" id="primary-detail-expanded-example-toolbar-select-checkbox-risk-restarted" name="primary-detail-expanded-example-toolbar-select-checkbox-risk-restarted" />
                                      <span class="pf-c-check__label">Restarted</span>
                                    </label>
                                  </fieldset>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="pf-c-toolbar__item">
                          <button class="pf-c-button pf-m-plain" type="button" aria-label="Edit">
                            <i class="fas fa-sort-amount-down" aria-hidden="true"></i>
                          </button>
                        </div>
                        <div class="pf-c-overflow-menu" id="overflow-menu-simple-additional-options-visible">
                          <div class="pf-c-overflow-menu__content pf-u-display-none pf-u-display-flex-on-lg">
                            <div class="pf-c-overflow-menu__group pf-m-button-group">
                              <div class="pf-c-overflow-menu__item">
                                <button class="pf-c-button pf-m-primary" type="button">Create instance</button>
                              </div>
                            </div>
                          </div>
                          <div class="pf-c-overflow-menu__control">
                            <div class="pf-c-dropdown">
                              <button class="pf-c-button pf-c-dropdown__toggle pf-m-plain" type="button" id="overflow-menu-simple-additional-options-visible-dropdown-toggle" aria-label="Dropdown with additional options">
                                <i class="fas fa-ellipsis-v" aria-hidden="true"></i>
                              </button>
                              <ul class="pf-c-dropdown__menu" aria-labelledby="overflow-menu-simple-additional-options-visible-dropdown-toggle" hidden>
                                <li>
                                  <button class="pf-c-dropdown__menu-item">Action 7</button>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class="pf-c-toolbar__item pf-m-pagination">
                          <div class="pf-c-pagination pf-m-compact">
                            <div class="pf-c-options-menu">
                              <div class="pf-c-options-menu__toggle pf-m-text pf-m-plain">
                                <span class="pf-c-options-menu__toggle-text">
                                  <b>1 - 10</b>&nbsp;of&nbsp;
                                  <b>37</b>
                                </span>
                                <button class="pf-c-options-menu__toggle-button" id="primary-detail-expanded-example-toolbar-top-pagination-toggle" aria-haspopup="listbox" aria-expanded="false" aria-label="Items per page">
                                  <span class="pf-c-options-menu__toggle-button-icon">
                                    <i class="fas fa-caret-down" aria-hidden="true"></i>
                                  </span>
                                </button>
                              </div>
                              <ul class="pf-c-options-menu__menu" aria-labelledby="primary-detail-expanded-example-toolbar-top-pagination-toggle" hidden>
                                <li>
                                  <button class="pf-c-options-menu__menu-item" type="button">5 per page</button>
                                </li>
                                <li>
                                  <button class="pf-c-options-menu__menu-item" type="button">10 per page
                                    <div class="pf-c-options-menu__menu-item-icon">
                                      <i class="fas fa-check" aria-hidden="true"></i>
                                    </div>
                                  </button>
                                </li>
                                <li>
                                  <button class="pf-c-options-menu__menu-item" type="button">20 per page</button>
                                </li>
                              </ul>
                            </div>
                            <nav class="pf-c-pagination__nav" aria-label="Toolbar top pagination">
                              <div class="pf-c-pagination__nav-control pf-m-prev">
                                <button class="pf-c-button pf-m-plain" type="button" disabled aria-label="Go to previous page">
                                  <i class="fas fa-angle-left" aria-hidden="true"></i>
                                </button>
                              </div>
                              <div class="pf-c-pagination__nav-control pf-m-next">
                                <button class="pf-c-button pf-m-plain" type="button" aria-label="Go to next page">
                                  <i class="fas fa-angle-right" aria-hidden="true"></i>
                                </button>
                              </div>
                            </nav>
                          </div>
                        </div>
                      </div>
                      <div class="pf-c-toolbar__expandable-content pf-m-hidden" id="primary-detail-expanded-example-toolbar-expandable-content" hidden></div>
                    </div>
                  </div>
                  <ul class="pf-c-data-list" role="list" aria-label="Simple data list example" id="primary-detail-expanded-example-data-list">
                    <li class="pf-c-data-list__item" aria-labelledby="primary-detail-expanded-example-data-list-item-1">
                      <div class="pf-c-data-list__item-row">
                        <div class="pf-c-data-list__item-content">
                          <div class="pf-c-data-list__cell pf-m-align-left">
                            <div class="pf-l-flex pf-m-column pf-m-space-items-md">
                              <div class="pf-l-flex pf-m-column pf-m-space-items-none">
                                <div class="pf-l-flex__item">
                                  <p id='primary-detail-expanded-example-data-list-item-1'>patternfly</p>
                                </div>
                                <div class="pf-l-flex__item">
                                  <small>Working repo for PatternFly 4
                                    <a>https://pf4.patternfly.org/</a>
                                  </small>
                                </div>
                              </div>
                              <div class="pf-l-flex pf-m-wrap">
                                <div class="pf-l-flex pf-m-space-items-xs">
                                  <div class="pf-l-flex__item">
                                    <i class="fas fa-code-branch" aria-hidden="true"></i>
                                  </div>
                                  <div class="pf-l-flex__item">
                                    <span>10</span>
                                  </div>
                                </div>
                                <div class="pf-l-flex pf-m-space-items-xs">
                                  <div class="pf-l-flex__item">
                                    <i class="fas fa-code" aria-hidden="true"></i>
                                  </div>
                                  <div class="pf-l-flex__item">
                                    <span>4</span>
                                  </div>
                                </div>
                                <div class="pf-l-flex pf-m-space-items-xs">
                                  <div class="pf-l-flex__item">
                                    <i class="fas fa-cube" aria-hidden="true"></i>
                                  </div>
                                  <div class="pf-l-flex__item">
                                    <span>5</span>
                                  </div>
                                </div>
                                <div class="pf-l-flex__item">Updated 2 days ago</div>
                              </div>
                            </div>
                          </div>
                          <div class="pf-c-data-list__cell pf-m-align-right pf-m-no-fill">
                            <button class="pf-c-button pf-m-secondary" type="button">Action</button>
                            <button class="pf-c-button pf-m-link" type="button">Link</button>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="pf-c-data-list__item" aria-labelledby="primary-detail-expanded-example-data-list-item-2">
                      <div class="pf-c-data-list__item-row">
                        <div class="pf-c-data-list__item-content">
                          <div class="pf-c-data-list__cell pf-m-align-left">
                            <div class="pf-l-flex pf-m-column pf-m-space-items-md">
                              <div class="pf-l-flex pf-m-column pf-m-space-items-none">
                                <div class="pf-l-flex__item">
                                  <p id='primary-detail-expanded-example-data-list-item-2'>patternfly-elements</p>
                                </div>
                                <div class="pf-l-flex__item">
                                  <small>PatternFly elements</small>
                                </div>
                              </div>
                              <div class="pf-l-flex pf-m-wrap">
                                <div class="pf-l-flex pf-m-space-items-xs">
                                  <div class="pf-l-flex__item">
                                    <i class="fas fa-code-branch" aria-hidden="true"></i>
                                  </div>
                                  <div class="pf-l-flex__item">
                                    <span>5</span>
                                  </div>
                                </div>
                                <div class="pf-l-flex pf-m-space-items-xs">
                                  <div class="pf-l-flex__item">
                                    <i class="fas fa-code" aria-hidden="true"></i>
                                  </div>
                                  <div class="pf-l-flex__item">
                                    <span>9</span>
                                  </div>
                                </div>
                                <div class="pf-l-flex pf-m-space-items-xs">
                                  <div class="pf-l-flex__item">
                                    <i class="fas fa-cube" aria-hidden="true"></i>
                                  </div>
                                  <div class="pf-l-flex__item">
                                    <span>2</span>
                                  </div>
                                </div>
                                <div class="pf-l-flex pf-m-space-items-xs">
                                  <div class="pf-l-flex__item">
                                    <i class="fas fa-check-circle" aria-hidden="true"></i>
                                  </div>
                                  <div class="pf-l-flex__item">
                                    <span>11</span>
                                  </div>
                                </div>
                                <div class="pf-l-flex pf-m-space-items-xs">
                                  <div class="pf-l-flex__item">
                                    <i class="fas fa-exclamation-triangle" aria-hidden="true"></i>
                                  </div>
                                  <div class="pf-l-flex__item">
                                    <span>4</span>
                                  </div>
                                </div>
                                <div class="pf-l-flex pf-m-space-items-xs">
                                  <div class="pf-l-flex__item">
                                    <i class="fas fa-times-circle" aria-hidden="true"></i>
                                  </div>
                                  <div class="pf-l-flex__item">
                                    <span>1</span>
                                  </div>
                                </div>
                                <div class="pf-l-flex__item">Updated 2 days ago</div>
                              </div>
                            </div>
                          </div>
                          <div class="pf-c-data-list__cell pf-m-align-right pf-m-no-fill">
                            <button class="pf-c-button pf-m-secondary" type="button">Action</button>
                            <button class="pf-c-button pf-m-link" type="button">Link</button>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="pf-c-data-list__item" aria-labelledby="primary-detail-expanded-example-data-list-item-3">
                      <div class="pf-c-data-list__item-row">
                        <div class="pf-c-data-list__item-content">
                          <div class="pf-c-data-list__cell pf-m-align-left">
                            <p id='primary-detail-expanded-example-data-list-item-3'>patternfly-unified-design-kit</p>
                          </div>
                          <div class="pf-c-data-list__cell pf-m-align-right pf-m-no-fill">
                            <button class="pf-c-button pf-m-secondary" type="button">Action</button>
                            <button class="pf-c-button pf-m-link" type="button">Link</button>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="pf-c-data-list__item" aria-labelledby="primary-detail-expanded-example-data-list-item-4">
                      <div class="pf-c-data-list__item-row">
                        <div class="pf-c-data-list__item-content">
                          <div class="pf-c-data-list__cell pf-m-align-left">
                            <div class="pf-l-flex pf-m-column pf-m-space-items-md">
                              <div class="pf-l-flex pf-m-column pf-m-space-items-none">
                                <div class="pf-l-flex__item">
                                  <p id='primary-detail-expanded-example-data-list-item-4'>patternfly</p>
                                </div>
                                <div class="pf-l-flex__item">
                                  <small>Working repo for PatternFly 4
                                    <a>https://pf4.patternfly.org/</a>
                                  </small>
                                </div>
                              </div>
                              <div class="pf-l-flex pf-m-wrap">
                                <div class="pf-l-flex pf-m-space-items-xs">
                                  <div class="pf-l-flex__item">
                                    <i class="fas fa-code-branch" aria-hidden="true"></i>
                                  </div>
                                  <div class="pf-l-flex__item">
                                    <span>10</span>
                                  </div>
                                </div>
                                <div class="pf-l-flex pf-m-space-items-xs">
                                  <div class="pf-l-flex__item">
                                    <i class="fas fa-code" aria-hidden="true"></i>
                                  </div>
                                  <div class="pf-l-flex__item">
                                    <span>4</span>
                                  </div>
                                </div>
                                <div class="pf-l-flex pf-m-space-items-xs">
                                  <div class="pf-l-flex__item">
                                    <i class="fas fa-cube" aria-hidden="true"></i>
                                  </div>
                                  <div class="pf-l-flex__item">
                                    <span>5</span>
                                  </div>
                                </div>
                                <div class="pf-l-flex__item">Updated 2 days ago</div>
                              </div>
                            </div>
                          </div>
                          <div class="pf-c-data-list__cell pf-m-align-right pf-m-no-fill">
                            <button class="pf-c-button pf-m-secondary" type="button">Action</button>
                            <button class="pf-c-button pf-m-link" type="button">Link</button>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="pf-c-data-list__item" aria-labelledby="primary-detail-expanded-example-data-list-item-5">
                      <div class="pf-c-data-list__item-row">
                        <div class="pf-c-data-list__item-content">
                          <div class="pf-c-data-list__cell pf-m-align-left">
                            <div class="pf-l-flex pf-m-column pf-m-space-items-md">
                              <div class="pf-l-flex pf-m-column pf-m-space-items-none">
                                <div class="pf-l-flex__item">
                                  <p id='primary-detail-expanded-example-data-list-item-5'>patternfly-elements</p>
                                </div>
                                <div class="pf-l-flex__item">
                                  <small>PatternFly elements</small>
                                </div>
                              </div>
                              <div class="pf-l-flex pf-m-wrap">
                                <div class="pf-l-flex pf-m-space-items-xs">
                                  <div class="pf-l-flex__item">
                                    <i class="fas fa-code-branch" aria-hidden="true"></i>
                                  </div>
                                  <div class="pf-l-flex__item">
                                    <span>5</span>
                                  </div>
                                </div>
                                <div class="pf-l-flex pf-m-space-items-xs">
                                  <div class="pf-l-flex__item">
                                    <i class="fas fa-code" aria-hidden="true"></i>
                                  </div>
                                  <div class="pf-l-flex__item">
                                    <span>9</span>
                                  </div>
                                </div>
                                <div class="pf-l-flex pf-m-space-items-xs">
                                  <div class="pf-l-flex__item">
                                    <i class="fas fa-cube" aria-hidden="true"></i>
                                  </div>
                                  <div class="pf-l-flex__item">
                                    <span>2</span>
                                  </div>
                                </div>
                                <div class="pf-l-flex pf-m-space-items-xs">
                                  <div class="pf-l-flex__item">
                                    <i class="fas fa-check-circle" aria-hidden="true"></i>
                                  </div>
                                  <div class="pf-l-flex__item">
                                    <span>11</span>
                                  </div>
                                </div>
                                <div class="pf-l-flex pf-m-space-items-xs">
                                  <div class="pf-l-flex__item">
                                    <i class="fas fa-exclamation-triangle" aria-hidden="true"></i>
                                  </div>
                                  <div class="pf-l-flex__item">
                                    <span>4</span>
                                  </div>
                                </div>
                                <div class="pf-l-flex pf-m-space-items-xs">
                                  <div class="pf-l-flex__item">
                                    <i class="fas fa-times-circle" aria-hidden="true"></i>
                                  </div>
                                  <div class="pf-l-flex__item">
                                    <span>1</span>
                                  </div>
                                </div>
                                <div class="pf-l-flex__item">Updated 2 days ago</div>
                              </div>
                            </div>
                          </div>
                          <div class="pf-c-data-list__cell pf-m-align-right pf-m-no-fill">
                            <button class="pf-c-button pf-m-secondary" type="button">Action</button>
                            <button class="pf-c-button pf-m-link" type="button">Link</button>
                          </div>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <!-- Panel -->
              <div class="pf-c-drawer__panel">
                <!-- Panel header -->
                <div class="pf-c-drawer__body">
                  <div class="pf-l-flex pf-m-column">
                    <div class="pf-l-flex__item">
                      <div class="pf-c-drawer__head">
                        <div class="pf-c-drawer__actions">
                          <div class="pf-c-drawer__close">
                            <button class="pf-c-button pf-m-plain" type="button" aria-label="Close drawer panel">
                              <i class="fas fa-times" aria-hidden="true"></i>
                            </button>
                          </div>
                        </div>
                        <h2 class="pf-c-title pf-m-lg" id="primary-detail-expanded-example-drawer-label">Node 2</h2>
                      </div>
                    </div>
                    <div class="pf-l-flex__item">
                      <a href="\#">siemur/test-space</a>
                    </div>
                  </div>
                </div>
                <!-- Tabs -->
                <div class="pf-c-drawer__body pf-m-no-padding">
                  <div class="pf-c-tabs pf-m-box pf-m-fill" id="primary-detail-expanded-example-tabs">
                    <button class="pf-c-tabs__scroll-button" aria-label="Scroll left">
                      <i class="fas fa-angle-left" aria-hidden="true"></i>
                    </button>
                    <ul class="pf-c-tabs__list">
                      <li class="pf-c-tabs__item pf-m-current">
                        <button class="pf-c-tabs__link" id="primary-detail-expanded-example-tabs-overview-link">
                          <span class="pf-c-tabs__item-text">Overview</span>
                        </button>
                      </li>
                      <li class="pf-c-tabs__item">
                        <button class="pf-c-tabs__link" id="primary-detail-expanded-example-tabs-activity-link">
                          <span class="pf-c-tabs__item-text">Activity</span>
                        </button>
                      </li>
                    </ul>
                    <button class="pf-c-tabs__scroll-button" aria-label="Scroll right">
                      <i class="fas fa-angle-right" aria-hidden="true"></i>
                    </button>
                  </div>
                </div>
                <!-- Tab content -->
                <div class="pf-c-drawer__body">
                  <section class="pf-c-tab-content" id="primary-detail-expanded-example-tab1-panel" role="tabpanel" tabindex="0">
                    <div class="pf-l-flex pf-m-column pf-m-space-items-lg">
                      <div class="pf-l-flex__item">
                        <p>The content of the drawer really is up to you. It could have form fields, definition lists, text lists, labels, charts, progress bars, etc. Spacing recommendation is 24px margins. You can put tabs in here, and can also make the drawer scrollable.</p>
                      </div>
                      <div class="pf-l-flex__item">
                        <div class="pf-c-progress pf-m-sm" id="primary-detail-expanded-example-progress-example1">
                          <div class="pf-c-progress__description" id="primary-detail-expanded-example-progress-example1-description">Title</div>
                          <div class="pf-c-progress__status" aria-hidden="true">
                            <span class="pf-c-progress__measure">33%</span>
                          </div>
                          <div class="pf-c-progress__bar" aria-label="Progress 1" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="33" aria-describedby="primary-detail-expanded-example-progress-example1-description">
                            <div class="pf-c-progress__indicator" style="width:33%;"></div>
                          </div>
                        </div>
                      </div>
                      <div class="pf-l-flex__item">
                        <div class="pf-c-progress pf-m-sm" id="primary-detail-expanded-example-progress-example2">
                          <div class="pf-c-progress__description" id="primary-detail-expanded-example-progress-example2-description">Title</div>
                          <div class="pf-c-progress__status" aria-hidden="true">
                            <span class="pf-c-progress__measure">66%</span>
                          </div>
                          <div class="pf-c-progress__bar" aria-label="Progress 2" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="66" aria-describedby="primary-detail-expanded-example-progress-example2-description">
                            <div class="pf-c-progress__indicator" style="width:66%;"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
                  <section class="pf-c-tab-content" id="primary-detail-expanded-example-tab2-panel" role="tabpanel" tabindex="0" hidden>Panel 2</section>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
    </div>
    </body>
    </html>
    "#;
    //"reMath"; //fs::read_to_string("hello.html").unwrap();

        let response = format!(
            "HTTP/1.1 200 OK\r\nContent-Length: {}\r\n\r\n{}",
            contents.len(),
            contents
        );

    return response;
}