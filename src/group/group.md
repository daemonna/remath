# GROUP

Group is a set equipped with a binary operation that combines any two elements to form a third element in such a way that three conditions called group axioms are satisfied, namely associativity, identity and invertibility.

A simple and compact way of specifying a group is by giving a set of elements whose repeated product produces the whole group. These are then called generators of the group and the set of elements is referred to as a generating set. This is a powerful and general method to specify a finite group, though for large groups it might be difficult to extract information from them about the group.

 All finite groups can be represented as permutation groups, which means they are always isomorphic to a subgroup of the symmetric group Subscript[S, n] of automorphisms of a set of n elements (Cayley's theorem).

The definition of a group doesn’t demand that gi * gj = gj * gi
