struct Tensor {
    cols: i32,
    rows: i32,
    data: Vec<f32>
}

impl Tensor {
    fn new(cols: i32, rows: i32) -> Matrix {
        Matrix {
            cols: cols,
            rows: rows,
            data: vec![0.0; cols * rows]
        }
    }
}