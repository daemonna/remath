# TENSORS

A geometric vector is an example of a contravariant vector because when changing basis, the components of the vector transform with the inverse of the basis transformation matrix (Example 1). It's easy to remember it as "contrary" to the basis matrix. As convention, we will usually label contravariant vectors with a superscript and write them as column vectors:

vα=⎡⎣⎢v0v1v2⎤⎦⎥(9)

# Non-Tensors

We can represent the height, width and length of a box as an ordered list of numbers: [10,20,15]. However, this is not a tensor because if we change our basis, the height, width and length of the box don't change, they stay the same. Tensors, however, have specific rules of how to change their representation when the basis changes. Therefore, this tuple is not a tensor.







<!-- 

I read somewhere that not all vectors are tensors. Can you give me an example?
This is perfectly true, but, in fact all tensors are vectors.

But then you have to be using the mathematical definition of the word vector as a member of a vector space.

Vectors in a vector space are (mathematical) objects that obey the vector space axioms, in particular they have a binary combination law that obeys the the associated Field.

So from this point of view definite integrals form a vector space where the vectors are definite integrals.

Fourier series form another example, where the vectors have the form asinx or bcosx.

------------------------------

Tensors of any order can be resented by matrices.

But

Not every matrix or even every square matrix represents a Tensor.

-------------------

I'm just learning tensors in detail myself but the secret here, I believe, is in the definition of a tensor ... "tensors are independent of a particular choice of coordinate system" or ... "tensors are invariant when the frame of reference is changed". So you have to know what a scalar or a vector are referring to before you know whether it is a tensor or not.
Assume you have two frames of reference: a stationary frame A and a moving frame B. If someone in each frame measures the temperature (T) of a point P the temperature will be the same. If, however, they measure the frequency (f) of the light coming from a point P the frequency will be different due to relativity (red or blue shifted depending on whether the B frame is moving away from or towards the point P). So T is invariant and therefore a scalar tensor (rank 0) whereas f varies and is not a tensor. So, the scalar 71 could be a tensor or not a tensor depending on what it is measuring.
Similarly with a vector it must be invariant to be a rank 1 tensor. So a vector showing velocity would vary depending on the observers and isn't a tensor. I can't off hand think of a vector that is a tensor ... maybe the force on an object observed by each observer.
Can anyone else with a deeper understanding of tensors let me know if I'm correct here?
This is basically correct. The difference between vector and tensor is that any collection of numbers partitioned in the proper way is a scalar, vector, matrix, etc.

Tensors on the other hand have to obey coordinate transformation rules and remain the same object independent of the choice of coordinate system.

A vector that isn't a tensor (your mom's age, the height of your house, how many kids you have)

A vector that is a tensor, given some electric field E, (Ex,Ey,Ez)


------------------------------------------------------------------------------

You may want to read "An Introduction to Tensors for Students of Physics and Engineering, Joseph C. Kolecki, Glenn Research Center, Cleveland, Ohio"

It is a well thought out introduction. He provides the following examples of vectors that are or are not tensors.

"Now, let V be the position vector extending from the origin of a coordinate space K to a particular point P, and V* be the position vector extending from the origin of another coordinate space K* to that same point. Assume that the origins of K and K* do not coincide; then V â‰  V*. The position vector is very definitely coordinate dependent and is not a tensor because it does not satisfy the condition of coordinate independence.
But suppose that V1 and V2 were position vectors of points P1 and P2 in K, and that V1* and V2* were position vectors to the same points P1 and P2 in K*. The vector extending from P1 to P2 must be the same vector in both systems. This vector is V2 â€“ V1 in K and V2* â€“ V1* in K*. Thus we have

V2 â€“V1 =V2*â€“V1*,

i.e., while the position vector itself is not a tensor, the difference between any two position vectors is a tensor of rank 1! Similarly, for any position vectors V and V*, dV = dV*; i.e., the differential of the position vector is a tensor of rank 1." -->