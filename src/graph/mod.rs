#[derive(Clone, Debug)]
pub struct Graph {
    pub name: String,
    pub points: Vec<Point>,
    pub colour: String
}

#[derive(Clone, Debug, Copy)]
pub struct Point {
    pub x: f64,
    pub y: f64,
}

impl Graph {
    pub fn new(name: String, colour: String) -> Self {
        Graph {
            name,
            points: Vec::new(),
            colour,
        }
    }
    pub fn add_point(&mut self, x: f64, y: f64) {
        self.points.push(Point { x, y });
    }
}